import {Component, OnInit} from '@angular/core';
import {Product} from '../Classes/Product';
import {Supply} from '../Classes/supply';
import {MatDialog} from '@angular/material/dialog';
import { MatFormFieldControl } from '@angular/material/form-field';
import {SupplyService} from '../services/supply.service';
import {ProductService} from '../services/product.service';
import {ProductFormComponent} from '../product-form/product-form.component';
import {StorageService} from '../services/storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-supply-form',
  templateUrl: './supply-form.component.html',
  styleUrls: ['./supply-form.component.css'],
})
export class SupplyFormComponent implements OnInit {
  supply: Supply;
  category: string;
  products: Product[];

  constructor(public dialog: MatDialog, private supplyService: SupplyService, public productService: ProductService,
              private storageService: StorageService, private router: Router) {
    this.supply = new Supply();
    this.supply.shopId = this.storageService.getShop().id;
  }

  ngOnInit(): void {}
  openDialog(): void {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(formResult => {
      this.productService.save(formResult).subscribe(result =>
      this.categorySelected(formResult.category));
    });
  }
  // tslint:disable-next-line:typedef
  onSubmit() {
    console.log('salvo la supply');
    console.log(this.supply);
    this.supplyService.save(this.supply).subscribe(result => this.router.navigate(['/yourShop']) );
 }

  backHome(): void{
    this.router.navigate([`/Homepage`]);
  }

  productSelected(productSelected: Product): void{
    this.supply.product = productSelected;
  }
  categorySelected(category: string): void{
    this.category = category;
    this.productService.findByCategory(category).subscribe(data => {
      console.log(category);
      if (data.length === 0) {
        this.products = data;
        this.openDialog();
      } else {
        this.products = data;
      }
    });
  }
}
