import { Component, OnInit } from '@angular/core';
import {Order} from '../Classes/order';
import {OrderService} from '../services/order.service';
import {StorageService} from '../services/storage.service';
import {User} from '../Classes/user';
import {SupplyService} from '../services/supply.service';
import {ShopService} from '../services/shop.service';
import {ChartItem} from '../Classes/chart-item';
import {OrderFrontEnd} from '../Classes/order-front-end';
import {ChartItemsFrontEnd} from '../Classes/chart-items-front-end';

@Component({
  selector: 'app-orders-made',
  templateUrl: './orders-made.component.html',
  styleUrls: ['./orders-made.component.css']
})
export class OrdersMadeComponent implements OnInit {

  user: User;
  ordersBackEnd: Order[];
  ordersFrontEnd: OrderFrontEnd[];
  tempOrder: OrderFrontEnd;

  // tslint:disable-next-line:max-line-length
  constructor(private orderService: OrderService, private storageService: StorageService, private supplyService: SupplyService, private shopService: ShopService) { }

  ngOnInit(): void {
    this.ordersBackEnd = [];
    this.ordersFrontEnd = [];
    this.tempOrder = new OrderFrontEnd();

    this.user = this.storageService.getUser();
    this.orderService.getOrdersMade(this.user).subscribe(ordersBackEnd => {
      this.ordersFrontEnd = this.convertOrders(ordersBackEnd);
    });
  }

  // we need to convert every order so we can show all the info
  // ( backend ony saves ids, not objects )
    convertOrders(ordersBack: Order[]): OrderFrontEnd[]{
    let ordersFront = [];
    for (const order of ordersBack) {
        const newOrder = new OrderFrontEnd();
        newOrder.id = order.id;
        newOrder.insertDate = this.convertDate(order.insertDate);
        newOrder.status = order.status;
        newOrder.totalPrice = order.totalPrice;
      // 22 agosto
        this.shopService.findByShopId(order.shopId).subscribe(shopsFound => { newOrder.shop = shopsFound; } );
      // this.shopService.findByIds([order.shopId]).subscribe(shopsFound => {
      //  newOrder.shop = shopsFound[0]; } );
        this.orderService.getChartItems(order.id).subscribe(itemsFound => {
          newOrder.items = this.convertItems(itemsFound);
        });
        ordersFront = ordersFront.concat(newOrder);
      }
    return ordersFront;
    }

    convertDate(backDate: any): string{
      const date = new Date(backDate);
      // tslint:disable-next-line:max-line-length
      return date.getDate().toString() + '/' + (date.getMonth() + 1).toString() + '/' + date.getFullYear().toString() + ' ' + date.getHours().toString() + ' : ' + date.getMinutes().toString();
    }

    convertItems(itemsBack: ChartItem[]): ChartItemsFrontEnd[]{
    let itemsFront = [];
    for (const item of itemsBack) {
        const newItem = new ChartItemsFrontEnd();
        newItem.quantity = item.quantity;
        this.supplyService.findSupplyById(item.supplyID).subscribe(supplyFound => {
          newItem.supply = supplyFound;
        });
        itemsFront = itemsFront.concat(newItem) ;
      }
    return itemsFront;
    }

}
