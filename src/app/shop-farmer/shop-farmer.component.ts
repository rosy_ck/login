import { Component, OnInit } from '@angular/core';
import {StorageService} from '../services/storage.service';
import {Shop} from '../Classes/shop';
import {Supply} from '../Classes/supply';
import {SupplyService} from '../services/supply.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-shop-farmer',
  templateUrl: './shop-farmer.component.html',
  styleUrls: ['./shop-farmer.component.css']
})
export class ShopFarmerComponent implements OnInit {
  shop: Shop;
  supplies: Supply[];
  updateMode: Map<bigint, boolean>;
  newQuantity: number;
  newPrice: number;

  // tslint:disable-next-line:max-line-length
  constructor(private storageService: StorageService, private supplyService: SupplyService, private router: Router, public dialog: MatDialog) {
    this.shop = this.storageService.getShop();
    this.updateMode = new Map();
    this.supplyService.findAll(this.shop).subscribe(data => {
      this.supplies = data;
      for (const supply of this.supplies) {
        this.updateMode.set(supply.id, false);
      }
    });
  }

  ngOnInit(): void {
  }

  addProduct(): void {
    this.router.navigate(['/addFornitura']);
  }

  update(supply: Supply): void {
    this.supplyService.updateSupply(supply).subscribe(data => {
      this.updateMode.set(data.id, false);
      this.openDialog();
    });

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialog, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(formResult => {
    });
  }
}

@Component({
  // tslint:disable-next-line:component-selector
    selector: 'confirmation-dialog',
    templateUrl: 'confirmation-dialog.html',
  })
// tslint:disable-next-line:component-class-suffix
  export class ConfirmationDialog {}

