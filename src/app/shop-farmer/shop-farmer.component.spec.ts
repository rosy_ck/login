import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopFarmerComponent } from './shop-farmer.component';

describe('ShopFarmerComponent', () => {
  let component: ShopFarmerComponent;
  let fixture: ComponentFixture<ShopFarmerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopFarmerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopFarmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
