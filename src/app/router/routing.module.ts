import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {SignOutComponent} from '../sign-out/sign-out.component';
import {AuthButtonComponent} from '../auth-button/auth-button.component';
import {AccountSettingsComponent} from '../account-settings/account-settings.component';
import {ProductFormComponent} from '../product-form/product-form.component';
import {HomeComponent} from '../home/home.component';
import {AddShopComponent} from '../add-shop/add-shop.component';
import {MapComponent} from '../map/map.component';
import {ShopListComponent} from '../shop-list/shop-list.component';
import {ShopInformationComponent} from '../shop-information/shop-information.component';
import {SupplyFormComponent} from '../supply-form/supply-form.component';
import {ShopFarmerComponent} from '../shop-farmer/shop-farmer.component';
import {OrdersMadeComponent} from '../orders-made/orders-made.component';
import {OrdersReceivedComponent} from '../orders-received/orders-received.component';


const routes: Routes = [
  { path: 'signout', component: SignOutComponent },
  { path: 'Homepage', component: HomeComponent},
  { path: 'signin', component: AuthButtonComponent},
  { path: 'formUser', component: AccountSettingsComponent},
  { path: 'addProduct', component: ProductFormComponent },
  { path: 'addFornitura', component: SupplyFormComponent },
  { path: 'c', component: AddShopComponent },
  { path: 'Maps', component: MapComponent },
  { path: 'addShop', component: AddShopComponent },
  { path: 'infoShop', component: ShopInformationComponent },
  {path: 'yourShop', component: ShopFarmerComponent},
  {path: 'shopList', component: ShopListComponent},
  {path: 'ordersMade', component: OrdersMadeComponent},
  {path: 'ordersReceived', component: OrdersReceivedComponent}
];

/*CommonModule,*/
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
