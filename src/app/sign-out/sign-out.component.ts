import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {StorageService} from '../services/storage.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.css']
})
export class SignOutComponent implements OnInit {
// gapi.auth.signOut()

  public gapiSetup = false; // marks if the gapi library has been loaded
  // @ts-ignore
  public authInstance: gapi.auth2.GoogleAuth;
  public error: string;
  // @ts-ignore
  public user: gapi.auth2.GoogleUser;
  public status: any;
  constructor( private router: Router, private storageService: StorageService, public dialogRef: MatDialogRef<SignOutComponent>) {}
  ngOnInit(): void {
  }

  async signOut(): Promise<void>{
    // @ts-ignore
    if (!this.gapiSetup) {
      await this.initGoogleAuth();
    }
    // @ts-ignore
    const oauth = gapi.auth2.getAuthInstance();
    const auth2 = oauth.currentUser.get();
    if (oauth != null){
      oauth.signOut().then(auth2.disconnect());
      this.status =  oauth.isSignedIn.get();
      this.storageService.DeleteUser();
    } else { this.status = 'ERRORE'; }
    this.storageService.DeleteUser();
    this.dialogRef.close('yes');
  }

  initGoogleAuth(): Promise<void> {
    //  Create a new Promise where the resolve function is the callback
    // passed to gapi.load
    const pload = new Promise((resolve) => {
      // @ts-ignore
      gapi.load('auth2', resolve);
    });
    // When the first promise resolves, it means we have gapi loaded
    // and that we can call gapi.init
    return pload.then(async () => {
      // @ts-ignore
      await gapi.auth2
        .init({client_id: '122541883796-6j055a8skt9793essobl203htjgr3rqh.apps.googleusercontent.com'})
        .then(auth => {
          this.gapiSetup = true;
          this.authInstance = auth;
        });
    });
  }

}
