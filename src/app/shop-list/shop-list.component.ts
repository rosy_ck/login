import { Component, OnInit } from '@angular/core';
import {MapService} from '../services/map.service';
import {Shop} from '../Classes/shop';
import {Router} from '@angular/router';
import {StorageService} from '../services/storage.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SupplyService} from '../services/supply.service';
import {ShopService} from '../services/shop.service';

@Component({
  selector: 'app-farmer-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.css']
})
export class ShopListComponent implements OnInit {
  shops: Shop[];
  searchMode: boolean;
  categories: FormGroup;
  searchSelectionTypes: boolean[];

  constructor(private mapService: MapService, private router: Router, private storageService: StorageService,
              fb: FormBuilder, private shopService: ShopService,  private supplyService: SupplyService) {
    this.categories = fb.group({
      fruit: false,
      vegetable: false,
      cheese: false,
      wine: false
    });
  }

  ngOnInit(): void {
    this.searchSelectionTypes = [false, false, false, false];
    this.searchMode = false;
    this.mapService.shops().subscribe(data => {
      this.shops = data;
    });
  }

  seeShop(shop: Shop): void{
    this.storageService.setShopForShopping(shop);
    this.router.navigate([`/infoShop`]);
  }

  initSearch(): void{
    console.log('sono in init');
    if (this.searchMode === true){
      this.shops = [];
    }
    else{
      this.mapService.shops().subscribe(data => {
        this.shops = data;
      });
    }
  }

  updateShops(): void{
    this.shops = [];
    let categoryToSearch = [];
    if (this.searchSelectionTypes[0] === true) {
      categoryToSearch = categoryToSearch.concat('fruit');
    }
    if (this.searchSelectionTypes[1] === true) {
      categoryToSearch = categoryToSearch.concat('vegetable');
    }
    if (this.searchSelectionTypes[2] === true) {
      categoryToSearch = categoryToSearch.concat('cheese');
    }
    if (this.searchSelectionTypes[3] === true) {
      categoryToSearch = categoryToSearch.concat('wine');
    }
    for (const category of categoryToSearch){
      if (category){
    this.supplyService.findShopByCategory(category).subscribe(shopsId => {
        this.shopService.findByIds(shopsId).subscribe(shops => {
          for (const shop in shops){
            if (!(shop in this.shops)){
              this.shops = this.shops.concat(Object.values(shops));
            }
          }
        });
      });
  }}}
}

