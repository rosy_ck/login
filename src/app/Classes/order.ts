import {ChartItem} from './chart-item';
import {User} from './user';
import {Shop} from './shop';


export class Order{
  id: bigint;
  shopId: bigint;
  userId: string;
  status: string;
  payed: boolean;
  paymentType: string;
  insertDate: any;
  totalPrice: number;
  items: ChartItem[];
}
