export class Product {
  id: bigint;
  name: string;
  type: string;
  category: string;
}
