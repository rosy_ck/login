export class Shop {
  id?: bigint;
  name?: string;
  hasDelivery?: boolean;
  hasTakeAway?: boolean;
  shopImg?: string;
  description?: string;
  address?: string;
  civicNumber?: string;
  city?: string;
  longitude: number;
  latitude: number;
  productTypes: string[];
}
