import {Product} from './Product';


export class Supply {
  id: bigint;
  shopId: bigint;
  product: Product;
  quantity: number;
  price: number;

}
