import {Supply} from './supply';

export class ChartItem {
  quantity: number;
  supplyID: bigint;
}
