
import {ChartItemsFrontEnd} from './chart-items-front-end';
import {Shop} from './shop';

export class OrderFrontEnd {
  id: bigint;
  shop: Shop;
  userId: string;
  status: string;
  payed: boolean;
  paymentType: string;
  insertDate: string;
  totalPrice: number;
  items: ChartItemsFrontEnd[];
}
