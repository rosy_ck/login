import {Shop} from './shop';

export class User {
  id?: string;
  name: string;
  email: string;
  surname?: string;
  telephone?: string;
  birthDate?: string;
  fiscalCode?: string;
  profileImg?: string;
  address?: string;
  civicNumber?: string;
  city?: string;
  role?: string;
  token?: string;
  idToken?: string;
  shop?: Shop;
}
