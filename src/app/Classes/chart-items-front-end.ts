import {Supply} from './supply';

export class ChartItemsFrontEnd {

  quantity: number;
  price: number;
  supply: Supply;

}
