import {Component, OnInit} from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import {fromLonLat, toLonLat} from 'ol/proj';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import {MapService} from '../services/map.service';
import {Shop} from '../Classes/shop';
import Overlay from 'ol/Overlay';
import {Router, RouterModule} from '@angular/router';
import {StorageService} from '../services/storage.service';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  constructor(private mapService: MapService, private router: Router, public storageService: StorageService) {
  }

  map: Map;
  features: Feature[];

  ngOnInit(): void {
    // get all shops
    this.mapService.shops().subscribe(shops => {
      this.creaMappa(shops);
    });
  }

  // inserisce gli indirizzi nella mappa
  creaMappa(shops: Shop[]): void {
    this.features = [];
    // crea una feature per ogni indirizzo
    for (const s of shops) {
      if (s.longitude != null && s.latitude != null) {
        this.features.push(new Feature({
          geometry: new Point(fromLonLat([s.longitude, s.latitude])),
          name: s.name,
          shopObject: s,
          style: new Style({
            image: new Icon({
              color: '#BADA55',
              crossOrigin: 'anonymous',
              // For Internet Explorer 11
              imgSize: [20, 20],
              src: 'https://www.w3schools.com/images/picture.jpg',
            }),
          })
        }));
      }
    }

    // per avere un pop-up ( che comparirà al click )
    const container = document.getElementById('popup');
    const content = document.getElementById('popup-content');
    const overlay = new Overlay({
      element: container,
      autoPan: true,
      autoPanAnimation: {
        duration: 250
      }
    });

    const vectorSource = new VectorSource({
      features: this.features,
    });
    const vectorlayer = new VectorLayer({
      source: vectorSource,
    });

    const rasterLayer = new TileLayer({
      source: new OSM()
    });

    this.map = new Map({
      overlays: [overlay],
      target: document.getElementById('mappa'),
      layers: [
        rasterLayer,
        vectorlayer
      ],
      view: new View({
        center: olProj.fromLonLat([7.500000, 44.780000]),
        zoom: 7
      })
    });

    this.map.on('singleclick', evt => {
      const coordinate = evt.coordinate;
      // tslint:disable-next-line:only-arrow-functions no-shadowed-variable typedef
      const feature = this.map.forEachFeatureAtPixel(evt.pixel, function(feature) {
        return feature;
      });
      if (feature) {
        if ( content.lastElementChild){content.removeChild(content.lastElementChild); }
        const button = document.createElement('button');
        button.innerHTML  = feature.values_.name;
        // button redirects to shop's page
        // tslint:disable-next-line:no-shadowed-variable
        button.addEventListener('click', evt => { this.storageService.setShopForShopping(feature.values_.shopObject);
                                                  this.router.navigate([`/infoShop`]); });
        content.appendChild(button);
        overlay.setPosition(coordinate);
    }});
  }

}



