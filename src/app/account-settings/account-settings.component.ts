import { Component, OnInit } from '@angular/core';
import {User} from '../Classes/user';
import {Router} from '@angular/router';
import {StorageService} from '../services/storage.service';
import {LoginServiceService} from '../services/login-service.service';

// add info when logging in for the first time

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.css']
})
export class AccountSettingsComponent implements OnInit {
  user: User;
  constructor(private storageService: StorageService, private loginService: LoginServiceService, private router: Router) {
    this.user = this.storageService.getUser();
    console.log(' Constructor storage service ritorna:');
    console.log(this.user);
  }

  ngOnInit(): void {
    this.user = this.storageService.getUser();
    console.log('INIT storage service ritorna:');
    console.log(this.user);
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    console.log(this.user);
    this.loginService.UpdateAccount(this.user).subscribe(updatedUser => {
      this.storageService.setUser(updatedUser);
      this.router.navigate(['/Homepage']);
    });
  }

}
