import {Component, Directive, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../Classes/user';
import {StorageService} from '../services/storage.service';
import {MatDialog} from '@angular/material/dialog';
import {SignOutComponent} from '../sign-out/sign-out.component';
import {AuthButtonComponent} from '../auth-button/auth-button.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  usershop: User;
  isUserLoggedIn: boolean ;
  actions: string[];

  // @ts-ignore

  constructor( private storageService: StorageService, private router: Router, public dialog: MatDialog) {
    if (this.storageService.getCookie('user')) {
      this.isUserLoggedIn = true;
    }else {
      this.isUserLoggedIn = false;
    }
  }

  ngOnInit(): void {

    this.storageService.isUserLoggedIn.subscribe(value => {
      this.isUserLoggedIn = value;
    });

    this.usershop = this.storageService.getUser();
    this.actions = this.getActions();
  }

  // getUserLogged(): void{
  //   this.storageService.isUserLoggedIn.subscribe(value => {
  //     this.isUserLoggedIn = value;
  //   });
  // }

  getActions(): string[]{
    this.storageService.isUserLoggedIn.subscribe(value => {
      this.isUserLoggedIn = value;
      console.log('DENTRO stampa di isUserLoggedIn : ' + this.isUserLoggedIn);
    });


    console.log('FUORI stampa di isUserLoggedIn : ' + this.isUserLoggedIn);
    this.usershop = this.storageService.getUser();
    if (this.isUserLoggedIn) {
      console.log(this.usershop);
      console.log(this.usershop.role);
      if (this.usershop.role === 'farmer') {
        if (this.storageService.getShop()) {
          return ['My shop', 'Ordini effettuati', 'Ordini ricevuti' , 'LogOut'];
        } else {
          return ['Add shop', 'Ordini effettuati', 'Ordini ricevuti' , 'LogOut'];
        }
      } else {
        return ['Ordini effettuati' , 'LogOut'];
      }
    }
    else{
      return ['Login'];
    }
  }

  actionSelected( action: string): void{
    switch (action) {

      case 'My shop':
        this.router.navigate([`/yourShop`]);
        break;

      case 'Add shop':
        this.router.navigate([`//addShop`]);
        break;

      case 'LogOut':
        const dialogRef = this.dialog.open(SignOutComponent, {
          width: '250px'
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result === 'yes') {this.actions = this.getActions(); this.router.navigate([`/Homepage`]); } });
        break;

      case 'Login':
        const dialogRefLogIn = this.dialog.open(AuthButtonComponent, {
          width: '250px'
        });
        dialogRefLogIn.afterClosed().subscribe(result => {
          if (result === 'yes') {this.actions = this.getActions(); }});
        break;

      case 'Ordini effettuati':
        this.router.navigate([`/ordersMade`]);
        break;

      case 'Ordini ricevuti':
        this.router.navigate([`/ordersReceived`]);
        break;
    }
  }

}
