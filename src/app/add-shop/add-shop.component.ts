import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Shop} from '../Classes/shop';
import {ShopService} from '../services/shop.service';
import {StorageService} from '../services/storage.service';
import {MapService} from '../services/map.service';
import {User} from '../Classes/user';


@Component({
  selector: 'app-add-shop',
  templateUrl: './add-shop.component.html',
  styleUrls: ['./add-shop.component.css']
})
export class AddShopComponent implements OnInit {
  shop: Shop;
  user: User;
  constructor(    private route: ActivatedRoute,
                  private router: Router,
                  private shopService: ShopService,
                  private mapService: MapService,
                  private storageService: StorageService) { this.shop = new Shop(); }

  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  onSubmit() {
    // @ts-ignore
    const fullAdress = this.shop.address + ' ' + this.shop.civicNumber + ' ' + this.shop.city ;
    // get coordinates for the inserted address
    this.mapService.getCoordinates(fullAdress).subscribe(
      (result: any) => {
        // obtained various possible geo points -> consider only the first one
        const data = result.data[0];
        this.shop.latitude = data.latitude;
        this.shop.longitude = data.longitude;
        // save shop for the user and memorize it in storageService
        this.shopService.save(this.shop, this.storageService.getUser()).subscribe((shopBackend: any) => {
          this.storageService.setShop(shopBackend);
          this.router.navigate(['/Homepage']);
        });
      });
  }

}
