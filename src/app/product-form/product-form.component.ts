import {Component} from '@angular/core';
import {Product} from '../Classes/Product';
import { MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent {

  product: Product;

  constructor(
    public dialogRef: MatDialogRef<ProductFormComponent>) {
    this.product = new Product();
}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
