import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Order} from '../Classes/order';


// account per sandobox paypal
// user: sb-g4747zc4639768@personal.example.com
// pw: o#0yI}xZ

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  paidFor = false;

  constructor(@Inject(MAT_DIALOG_DATA) public order: Order, public dialogRef: MatDialogRef<PaymentComponent>) {}

  ngOnInit(): void {
    // @ts-ignore
    paypal.Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: this.order.id,
                amount: {
                  currency_code: 'USD',
                  value: this.order.totalPrice
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          this.paidFor = true;
          console.log(order);
          this.dialogRef.close('yes');
        },
        onError: err => {
          console.log(err);
        }
      })
      .render(this.paypalElement.nativeElement);
  }
}
