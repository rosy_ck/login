import {Component, OnInit} from '@angular/core';
import {Supply} from '../Classes/supply';
import {SupplyService} from '../services/supply.service';
import {Shop} from '../Classes/shop';
import {ChartItem} from '../Classes/chart-item';
import {StorageService} from '../services/storage.service';
import {MatDialog} from '@angular/material/dialog';
import {OrderService} from '../services/order.service';
import {Order} from '../Classes/order';
import {ChartItemsFrontEnd} from '../Classes/chart-items-front-end';
import {PaymentComponent} from '../payment/payment.component';
import {Router} from '@angular/router';



@Component({
  selector: 'app-shop-information',
  templateUrl: './shop-information.component.html',
  styleUrls: ['./shop-information.component.css']
})
export class ShopInformationComponent implements OnInit {
  shop: Shop;
  supplies: Supply[];
  chartItems: ChartItem[];
  chartItemsFrontEnd: ChartItemsFrontEnd[];
  qtyTemp: number;
  buyMode: Map<bigint, boolean>;
  totalPrice: number;
  payment: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private supplyService: SupplyService, private storage: StorageService, public dialog: MatDialog, private orderService: OrderService) {
    this.chartItems = [];
    this.chartItemsFrontEnd = [];
    this.totalPrice = 0;
    this.payment = false;
    this.buyMode = new Map();
  }

  ngOnInit(): void {
    this.shop = this.storage.GetShopForShopping();
    this.supplyService.findAll(this.shop).subscribe(data => {
      this.supplies = data;
      for (const supply of this.supplies) {
        this.buyMode.set(supply.id, false);
      }
    });
  }

 buy(supply: Supply, qty: number, price: number): void {
    const newItem = new ChartItem();
    const newItemFrontEnd = new ChartItemsFrontEnd();
    newItem.supplyID = supply.id;
    newItem.quantity = qty;
    newItemFrontEnd.supply = supply;
    newItemFrontEnd.quantity = qty;
    newItemFrontEnd.price = price * qty;
    this.chartItems.push(newItem);
    this.chartItemsFrontEnd.push(newItemFrontEnd);
    this.totalPrice = this.totalPrice + newItemFrontEnd.price;
    this.qtyTemp = null;
    this.buyMode.set (supply.id, false);
    this.openDialog();
 }

  confirm(shippingMethod: string): void {
    if (this.payment === false) {
      const newOrder = this.createOrderBackEnd();
      newOrder.payed = false;
      this.orderService.createOrder(newOrder).subscribe(data => {
        console.log('RISULTATI : ');
        console.log(data);
        this.openDialogOrderConfirmation();
      });
    }
    else{
      this.openDialogPayment();
    }
  }

  createOrderBackEnd(): Order{
    const newOrder = new Order();
    newOrder.items = this.chartItems;
    newOrder.status = 'created';
    newOrder.totalPrice = this.totalPrice;
    newOrder.insertDate = Date();
    newOrder.userId = this.storage.getUser().id;
    newOrder.shopId = this.shop.id;
    console.log(newOrder);
    return newOrder;
  }

  openDialogPayment(): void {
    const dialogRef = this.dialog.open(PaymentComponent, {
      width: '250px',
      data: this.createOrderBackEnd()
    });

    dialogRef.afterClosed().subscribe(paymentResult => {
      if (paymentResult === 'yes'){
        const newOrder = this.createOrderBackEnd();
        newOrder.status = 'payed';
        newOrder.payed = true;
        console.log('STATO');
        console.log(newOrder.status);
        this.orderService.createOrder(newOrder).subscribe(data => {
          console.log('RISULTATI : ');
          console.log(data);
          this.openDialogOrderConfirmation();
        });
      }
    });
  }

  openDialogOrderConfirmation(): void {
    const dialogRef = this.dialog.open(OrderConfirmationDialog, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(formResult => {
      this.router.navigate([`/Homepage`]);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddChartItemConfirmationDialog, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(formResult => {
    });
  }
}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'addChartItemConfirmation-dialog',
  templateUrl: 'addChartItemConfirmation-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class AddChartItemConfirmationDialog {}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'orderConfirmation-dialog',
  templateUrl: 'orderConfirmation-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class OrderConfirmationDialog {}

