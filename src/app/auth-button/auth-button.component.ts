import {Component, NgZone, OnInit} from '@angular/core';
import {User} from '../Classes/user';
import {LoginServiceService} from '../services/login-service.service';
import {Router} from '@angular/router';
import {StorageService} from '../services/storage.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-auth-button',
  templateUrl: './auth-button.component.html',
  styleUrls: ['./auth-button.component.css']
})

export class AuthButtonComponent implements OnInit {
  public gapiSetup = false; // marks if the gapi library has been loaded
  // @ts-ignore
  public authInstance: gapi.auth2.GoogleAuth;
  public error: string;
  // @ts-ignore
  public user: gapi.auth2.GoogleUser;
  usershop: User;

  constructor(private loginService: LoginServiceService,
              private zone: NgZone,
              private router: Router,
              private storageService: StorageService,
              public dialogRef: MatDialogRef<AuthButtonComponent>) {
  }

  // tslint:disable-next-line:typedef
  async ngOnInit() {
    if (await this.checkIfUserAuthenticated()) {
      this.user = this.authInstance.currentUser.get();
      this.usershop = {
        name: this.user.getBasicProfile().getName(),
        email: this.user.getBasicProfile().getEmail(),
        profileImg: this.user.getBasicProfile().getImageUrl(),
        token: this.user.getAuthResponse(true).access_token
      };
      this.loginService.GetUser(this.usershop).subscribe(userRes => {
        this.usershop = userRes;
        this.storageService.setUser(this.usershop);
        this.ControlloShop(userRes);
        this.storageService.isUserLoggedIn.next(true);
        this.router.navigate([`/Homepage`]);
      });
    }
  }

  async checkIfUserAuthenticated(): Promise<boolean> {
    // Initialize gapi if not done yet
    if (!this.gapiSetup) {
      await this.initGoogleAuth();
    }
    return this.authInstance.isSignedIn.get();
  }

  // @ts-ignore
  authenticate(): Promise<gapi.auth2.GoogleUser> {
    // Initialize gapi if not done yet
    if (!this.gapiSetup) {
      this.initGoogleAuth();
    }

    // Resolve or reject signin Promise
    return new Promise(async () => {
      // tslint:disable-next-line:typedef
      await this.authInstance.signIn().then(user => {
        this.usershop = {
          name: user.getBasicProfile().getName(),
          email: user.getBasicProfile().getEmail(),
          profileImg: user.getBasicProfile().getImageUrl(),
          token: user.getAuthResponse(true).access_token
        };
        this.loginService.CreateAccount(this.usershop).subscribe((res: any) => {
          // this.storageService.setUser(this.usershop);
          this.loginService.GetUser(this.usershop).subscribe(userRes => {
            console.log('sono dopo get user');
            this.usershop = userRes;
            this.storageService.setUser(userRes);
            console.log('dopo aver salvato in storage');
            this.ControlloShop(this.usershop);
            this.storageService.isUserLoggedIn.next(true);
            if (res.toString() === 'new user') {
              console.log(this.user);
              // this.zone.run(() => {  this.router.navigate([`/formUser`]); });
              this.router.navigate([`/formUser`]);
            } else {
              // this.zone.run(() => { this.router.navigate([`/Homepage`]); });
              this.router.navigate([`/Homepage`]);
            }
          });
        });
      });
      this.dialogRef.close('yes');
    });

  }

  initGoogleAuth(): Promise<void> {
    //  Create a new Promise where the resolve function is the callback
    // passed to gapi.load
    const pload = new Promise((resolve) => {
      // @ts-ignore
      gapi.load('auth2', resolve);
    });
    // When the first promise resolves, it means we have gapi loaded
    // and that we can call gapi.init
    return pload.then(async () => {
      // @ts-ignore
      await gapi.auth2
        .init({client_id: '122541883796-6j055a8skt9793essobl203htjgr3rqh.apps.googleusercontent.com'})
        .then(auth => {
          this.gapiSetup = true;
          this.authInstance = auth;
        });
    });
  }

  ControlloShop(userRes: any): void {
    // if (this.usershop.role === 'farmer') {
    if (userRes.role === 'farmer') {
      console.log('Se sei un farmer batti le mani');
      if (userRes.userShop) {
        this.storageService.setShop(userRes.userShop);
        console.log('Settato?');
      }
    }
    console.log('Non battere le mani');
  }
}




