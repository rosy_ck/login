import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { AuthButtonComponent } from './auth-button/auth-button.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import {RoutingModule} from './router/routing.module';
import {MaterialModule} from './material/material.module';
import {HttpClientModule} from '@angular/common/http';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { HomeComponent } from './home/home.component';
import {MatCardModule} from '@angular/material/card';
import { AddShopComponent } from './add-shop/add-shop.component';
import { MapComponent } from './map/map.component';
import { ShopListComponent } from './shop-list/shop-list.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { PaymentComponent } from './payment/payment.component';
import {AddChartItemConfirmationDialog, ShopInformationComponent} from './shop-information/shop-information.component';
import { SupplyFormComponent } from './supply-form/supply-form.component';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatNativeDateModule, MatOptionModule} from '@angular/material/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import { FlexLayoutModule } from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ConfirmationDialog, ShopFarmerComponent} from './shop-farmer/shop-farmer.component';
import {RouterModule} from '@angular/router';
import {User} from './Classes/user';
import {StorageService} from './services/storage.service';
import { OrdersReceivedComponent } from './orders-received/orders-received.component';
import { OrdersMadeComponent } from './orders-made/orders-made.component';
import {OrderConfirmationDialog} from './shop-information/shop-information.component';


@NgModule({
  declarations: [
    AppComponent,
    AuthButtonComponent,
    SignOutComponent,
    AccountSettingsComponent,
    HeaderComponent,
    HomeComponent,
    ProductFormComponent,
    AddShopComponent,
    MapComponent,
    ShopListComponent,
    PaymentComponent,
    ShopInformationComponent,
    SupplyFormComponent,
    ShopFarmerComponent,
    ConfirmationDialog,
    AddChartItemConfirmationDialog,
    OrderConfirmationDialog,
    OrdersReceivedComponent,
    OrdersMadeComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatGridListModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatCheckboxModule,
    MatDividerModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatRadioModule,
    MatNativeDateModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  /* exports: [MatButtonModule],*/
  providers: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
