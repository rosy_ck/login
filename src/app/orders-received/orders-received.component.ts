import { Component, OnInit } from '@angular/core';
import {User} from '../Classes/user';
import {Order} from '../Classes/order';
import {OrderService} from '../services/order.service';
import {StorageService} from '../services/storage.service';
import {OrderFrontEnd} from '../Classes/order-front-end';
import {ChartItem} from '../Classes/chart-item';
import {ChartItemsFrontEnd} from '../Classes/chart-items-front-end';
import {ShopService} from '../services/shop.service';
import {SupplyService} from '../services/supply.service';
import {Shop} from '../Classes/shop';

@Component({
  selector: 'app-orders-received',
  templateUrl: './orders-received.component.html',
  styleUrls: ['./orders-received.component.css']
})
export class OrdersReceivedComponent implements OnInit {


  shop: Shop;
  ordersBackEnd: Order[];
  ordersFrontEnd: OrderFrontEnd[];
  tempOrder: OrderFrontEnd;

  // tslint:disable-next-line:max-line-length
  constructor(private orderService: OrderService, private storageService: StorageService, private shopService: ShopService, private supplyService: SupplyService) { }

  ngOnInit(): void {
    this.ordersBackEnd = [];
    this.ordersFrontEnd = [];
    this.tempOrder = new OrderFrontEnd();

    this.shop = this.storageService.getShop();
    this.orderService.getOrdersReceived(this.shop).subscribe(ordersBackEnd => {
      this.ordersFrontEnd = this.convertOrders(ordersBackEnd);
    });
  }

  updateOrderState(orderId: bigint, newState: string): void{
    this.orderService.updateOrder(orderId, newState).subscribe();
  }

  // we need to convert every order so we can show all the info
  // ( backend ony saves ids, not objects )
  convertOrders(ordersBack: Order[]): OrderFrontEnd[]{
    let ordersFront = [];
    for (const order of ordersBack) {
      const newOrder = new OrderFrontEnd();
      newOrder.id = order.id;
      newOrder.insertDate = this.convertDate(order.insertDate);
      newOrder.status = order.status;
      newOrder.totalPrice = order.totalPrice;
      // 22 agosto
      this.shopService.findByShopId(order.shopId).subscribe(shopsFound => { newOrder.shop = shopsFound; } );
      console.log('Shop found, go ahead');
      /*this.shopService.findByIds([order.shopId]).subscribe(shopsFound => {
        newOrder.shop = shopsFound[0]; } );*/
      this.orderService.getChartItems(order.id).subscribe(itemsFound => {
        console.log('getChartItems: ' + itemsFound);
        newOrder.items = this.convertItems(itemsFound);
      });
      ordersFront = ordersFront.concat(newOrder);
    }
    return ordersFront;
  }

  convertDate(backDate: any): string{
    const date = new Date(backDate);
    // tslint:disable-next-line:max-line-length
    return date.getDate().toString() + '/' + (date.getMonth() + 1).toString() + '/' + date.getFullYear().toString() + ' ' + date.getHours().toString() + ' : ' + date.getMinutes().toString();
  }

  convertItems(itemsBack: ChartItem[]): ChartItemsFrontEnd[]{
    let itemsFront = [];
    for (const item of itemsBack) {
      const newItem = new ChartItemsFrontEnd();
      newItem.quantity = item.quantity;
      this.supplyService.findSupplyById(item.supplyID).subscribe(supplyFound => {
        newItem.supply = supplyFound;
      });
      itemsFront = itemsFront.concat(newItem) ;
    }
    return itemsFront;
  }

}




