import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  url;

  constructor(private  http: HttpClient) { }

  // tslint:disable-next-line:typedef
  CreateAccount(responce)
  {
    this.url =  'http://localhost:8080/api/v1/user';
    return this.http.post(this.url, responce, {headers : new HttpHeaders ({'Content-Type' : 'application/json'}), responseType: 'text'});
  }
  UpdateAccount(user): Observable<any>{
    this.url =  'http://localhost:8080/api/v1/user/update';
    console.log('user :');
    console.log(user);
    return this.http.post(this.url, user);
  }
  GetUser(user): Observable<any>{
    this.url = 'http://localhost:8080/api/v1/user/find';
    return this.http.post(this.url, user.email);
  }
}
