import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Shop} from '../Classes/shop';


@Injectable({
  providedIn: 'root'
})
export class MapService {
  private baseUrl = 'http://api.positionstack.com/v1';
  private backendUrl = 'http://localhost:8080/api/v1/shops';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:typedef
  public getCoordinates(query: string) {
    query = query.trim();
    const options = query ?
      {
        params: new HttpParams()
          .set('access_key', 'c1528f31de95afb866f6e21ebc7260c5')
          .set('query', query)
          .set('limit', '10')
          .set('output', 'json')
      } : {};

    return this.http.get(
      this.baseUrl + '/forward',
      options
    );
  }

  public shops(): Observable<Shop[]> {
    return this.http.get<Shop[]>(this.backendUrl);
  }


}
