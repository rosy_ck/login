import { Injectable } from '@angular/core';
import {User} from '../Classes/user';
import {Shop} from '../Classes/shop';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor() {
     // if (this.getCookie('user')) {
     //   this.isUserLoggedIn.next(true);
     // }
  }

  public getUser(): User {
    if (this.getCookie('user')) {
      return JSON.parse(this.getCookie('user'));
    } else {
      return null;
    }
  }
  public getShop(): Shop{
    if (this.getCookie('shop')) {
      return JSON.parse(this.getCookie('shop'));
    }
    else {
      return null;
    }
  }
  public setShop(shop: Shop): void {
    this.setCookie('shop', JSON.stringify(shop));
  }
  public GetShopForShopping(): Shop{
    return JSON.parse(this.getCookie('shop'));
  }
  public setShopForShopping(shop: Shop): void {
    this.setCookie('shop', JSON.stringify(shop));
  }
  public setUser(user: User): void {
    this.isUserLoggedIn.next(true);
    this.setCookie('user', JSON.stringify(user));
  }
  public DeleteUser(): void{
    this.deleteCookie('user');
  }
  // tslint:disable-next-line:typedef
  public setCookie(name: string, val: string) {
    const date = new Date();
    const value = val;

    // Set it expire in 7 days
    date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000));

    // Set it
    document.cookie = name + '=' + value + '; expires=' + date.toUTCString() + '; path=/';
  }

  // tslint:disable-next-line:typedef
  public getCookie(name: string) {
    const value = '; ' + document.cookie;
    const parts = value.split('; ' + name + '=');

    if (parts.length === 2) {
      return parts.pop().split(';').shift();
    }
  }

  // tslint:disable-next-line:typedef
   private deleteCookie(name: string) {
    const date = new Date();

    // Set it expire in -1 days
    date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
    this.isUserLoggedIn.next(false);
    // Set it
    document.cookie = name + '=; expires=' + date.toUTCString() + '; path=/';
  }
}
