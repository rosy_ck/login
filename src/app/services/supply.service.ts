import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Supply} from '../Classes/supply';
import {Shop} from '../Classes/shop';


@Injectable({
  providedIn: 'root'
})
export class SupplyService {
  private SupplyUrl: string;

  constructor(private http: HttpClient) {
    this.SupplyUrl = 'http://localhost:8083/api/v1/supply';
  }

  public findAll(shop: Shop): Observable<Supply[]> {
    return this.http.get<Supply[]>(this.SupplyUrl + '/' + shop.id);
  }
  // tslint:disable-next-line:typedef
  public save(supply: Supply) {
    return this.http.post<Supply>(this.SupplyUrl + '/add', supply);
  }

  // tslint:disable-next-line:typedef
  public updateSupply(supply: Supply) {
    console.log(this.SupplyUrl + '/update');
    return this.http.post<Supply>(this.SupplyUrl + '/update', supply);
  }

  // tslint:disable-next-line:typedef
  public findShopByCategory(category: string) {
    return this.http.post(this.SupplyUrl + '/findShopByCategory', category);
  }


  public findSupplyById(supplyId: bigint): Observable<any>{
    return this.http.post(this.SupplyUrl + '/findSupplyById', supplyId);
  }
}
