import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {Product} from '../Classes/Product';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly productUrl: string;

  constructor(private http: HttpClient) {
    this.productUrl = 'http://localhost:8080/api/v1/products';
  }

  public findAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productUrl);
  }

  public findByCategory(category: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.productUrl + '/category?category=' + category);
  }

  // tslint:disable-next-line:typedef
  public save(product: Product) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<Product>(this.productUrl, product);
  }
}
