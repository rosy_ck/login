import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product} from '../Classes/Product';
import {Shop} from '../Classes/shop';
import {User} from '../Classes/user';
import {Observable} from 'rxjs';
import {Supply} from '../Classes/supply';



@Injectable({
  providedIn: 'root'
})
export class ShopService {
  private shopUrl: string;

  constructor(private http: HttpClient) {
    this.shopUrl = 'http://localhost:8080/api/v1/shop';
  }
  // tslint:disable-next-line:typedef
  public save(shop: Shop, user: User) {
    const data = {Shop: shop, userEmail: user.email};
    return this.http.post(this.shopUrl, data);
  }

  // tslint:disable-next-line:typedef
  public findByIds(shopsId: any){
    console.log(shopsId);
    return this.http.post(this.shopUrl + '/findByIds', shopsId);
  }

  // Aggiunto 22 agosto
  // tslint:disable-next-line:typedef
  public findByShopId(shopId: any){
    console.log('findByShopId: ' + shopId);
    return this.http.get<Shop>('http://localhost:8080/api/v1/shop/shopId?shopId=' + shopId);
    // return this.http.post('http://localhost:8085/api/v1/shop/shopId?shopId=' + shopId, null);
  }

}
