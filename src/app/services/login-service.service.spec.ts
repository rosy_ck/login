import { TestBed } from '@angular/core/testing';

import { LoginServiceService } from './login-service.service';

// @ts-ignore
describe('LoginServiceService', () => {
  let service: LoginServiceService;

  // @ts-ignore
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginServiceService);
  });

  // @ts-ignore
  it('should be created', () => {
    // @ts-ignore
    expect(service).toBeTruthy();
  });
});
