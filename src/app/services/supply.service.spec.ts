import { TestBed } from '@angular/core/testing';

import { FornituraService } from './supply.service';

describe('FornituraService', () => {
  let service: FornituraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FornituraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
