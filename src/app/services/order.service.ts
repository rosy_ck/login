import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  url;

  constructor(private  http: HttpClient) { }

  // tslint:disable-next-line:typedef
  createOrder(order)
  {
    this.url =  'http://localhost:8080/api/v1/order';
    // tslint:disable-next-line:max-line-length
    const data = {Order: {shopId: order.shopId , userId: order.userId, status: order.status, payed: order.payed, totalPrice: order.totalPrice}, OrderItem: order.items};
    return this.http.post(this.url, data);
  }
  // tslint:disable-next-line:typedef
  updateOrder(orderId, newState)
  {
    this.url =  'http://localhost:8080/api/v1/order/updateStatus';
    // tslint:disable-next-line:max-line-length
    return this.http.post(this.url, {id: orderId, newStatus: newState}, {headers : new HttpHeaders ({'Content-Type' : 'application/json'}), responseType: 'text'});
  }

  getOrdersMade(user): Observable<any>{
    this.url = 'http://localhost:8080/api/v1/order/findDone';
    return this.http.post(this.url, user.id);
  }

  getOrdersReceived(shop): Observable<any>{
    this.url = 'http://localhost:8080/api/v1/order/findReceived';
    return this.http.post(this.url, shop.id);
  }

  getChartItems(orderId): Observable<any>{
    const url = '/api/v1/ElementCart/findByOrderId';
    return this.http.post(url, orderId);
  }

}


